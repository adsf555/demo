package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.example.demo.auth.AuthEntryPointJwt;
import com.example.demo.auth.JwtAuthenticationFilter;
import com.example.demo.imp.UserDetailsServiceImp;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

//	@Autowired
//	private CustomAuthenticationProvider customAuthenticationProvider;
	/*
	 * 不進行驗證的url
	 */
	private static final String[] AUTH_LIST = { "/v2/api-docs", "/configuration/ui", "/swagger-resources/**",
			"/configuration/security", "/swagger-ui.html", "/api/auth/**", "/api/test/**" };

	@Autowired
	private AuthEntryPointJwt authEntryPointJwt;

	@Autowired
	UserDetailsServiceImp userDetailsServiceImp;

	@Bean
	public JwtAuthenticationFilter jwtAuthenticationFilter() {
		return new JwtAuthenticationFilter();
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsServiceImp);
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}

	@Bean
	public AuthenticationManager authenticationManager(AuthenticationConfiguration authConfig) throws Exception {
		return authConfig.getAuthenticationManager();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// 因為用jwt驗證，因此關閉跨來源資源共用
		http.cors().and()
				// 關閉跨域請求
				.csrf().disable()
				// 增加錯誤訊息的作法
				.exceptionHandling().authenticationEntryPoint(authEntryPointJwt).and()
				// 關閉session
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
				// AUTH_LIST內的請求放行
				.authorizeRequests().antMatchers(AUTH_LIST).permitAll()
				// 對請求進行驗證
				.anyRequest().authenticated();
		
		http.authenticationProvider(authenticationProvider());
		// 增加JWT的驗證
		http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

	}

//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		// 使用自定義的驗證
//		auth.authenticationProvider(customAuthenticationProvider);
//	}

}
