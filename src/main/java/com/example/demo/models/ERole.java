package com.example.demo.models;

public enum ERole {
	ROLE_USER,
    ROLE_OFFICE,
    ROLE_ADMIN
}
