package com.example.demo.util;

import java.sql.Date;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.example.demo.auth.UserDetailsImp;
import com.fasterxml.jackson.core.JsonProcessingException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtUtil {
	private static final Logger logger = LoggerFactory.getLogger(JwtUtil.class);
	

	private static final String key = "Owen";
	private static final String TOKEN_PREFIX = "Bearer "; // Token前缀
	private static final SecretKey signingKey = new SecretKeySpec(Base64.getDecoder().decode(key),
			SignatureAlgorithm.HS256.getJcaName());// 加解密的key

	/*
	 * 產生jwt
	 */
	public String generateJwt(Authentication user) throws JsonProcessingException {

		long expiration = System.currentTimeMillis() + 30 * 60 * 1000;
		String uuid = UUID.randomUUID().toString().replace("-", "");
		UserDetailsImp userPrincipal = (UserDetailsImp) user.getPrincipal();

		Map<String, Object> claims = new HashMap<>();
		claims.put("uuid", uuid);
		claims.put("username", userPrincipal.getUsername());

		return createToke(claims, expiration);
	}

	/*
	 * 製作token
	 */
	public String createToke(Map<String, Object> claims, Long expiration) {

		JwtBuilder jwtBuilder = Jwts.builder();
		// user資訊
		// 3 種聲明 (Claims)=Reserved (註冊聲明), Public (公開聲明), Private (私有聲明)
		jwtBuilder.setClaims(claims);
		// 時效性
		jwtBuilder.setExpiration(new Date(expiration));
		// 簽名檔
		jwtBuilder.signWith(SignatureAlgorithm.HS256, signingKey);

		return jwtBuilder.compact();
	}

	/*
	 * token轉回claims
	 */
	public Claims parseJwt(String token) {
		try {
			return Jwts.parser().setSigningKey(signingKey).parseClaimsJws(token.replace(TOKEN_PREFIX, "").trim())
					.getBody();
		} catch (SignatureException e) {
			logger.error("Invalid JWT signature: {}", e.getMessage());
		} catch (MalformedJwtException e) {
			logger.error("Invalid JWT token: {}", e.getMessage());
		} catch (ExpiredJwtException e) {
			logger.error("JWT token is expired: {}", e.getMessage());
		} catch (UnsupportedJwtException e) {
			logger.error("JWT token is unsupported: {}", e.getMessage());
		} catch (IllegalArgumentException e) {
			logger.error("JWT claims string is empty: {}", e.getMessage());
		}
		return null;
	}
}
