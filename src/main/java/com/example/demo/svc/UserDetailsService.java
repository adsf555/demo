package com.example.demo.svc;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;


public interface UserDetailsService {

	public List<UserDetails> getUserAll();

	public UserDetails loadUserByAccount(String Account);

}
